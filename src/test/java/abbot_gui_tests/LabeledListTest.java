package abbot_gui_tests;

import abbot.finder.Matcher;
import abbot.finder.matchers.ClassMatcher;
import abbot.tester.JListLocation;
import abbot.tester.JListTester;
import gui_samples.LabeledList;
import junit.extensions.abbot.ComponentTestFixture;
import org.junit.Test;

import javax.swing.*;
import java.awt.*;

public class LabeledListTest extends ComponentTestFixture {

    public LabeledListTest(String name) { super(name); }

    @Test
    public void testLabelChangedOnSelectionChange() throws Throwable {

        String[] contents = { "one", "two", "three" };
        final LabeledList labeledList = new LabeledList(contents);
        showFrame(labeledList);


        Component list = getFinder().find(new ClassMatcher(JList.class));
        JListTester tester = new JListTester();

        JLabel label = (JLabel)getFinder().find(labeledList, new Matcher() {
            public boolean matches(Component c) {
                return c.getClass().equals(JLabel.class)
                        && c.getParent() == labeledList;
            }
        });


        tester.actionSelectRow(list, new JListLocation(1));
        assertEquals("Wrong label after selection", "Selected: two", label.getText());

        tester.actionSelectRow(list, new JListLocation(2));
        assertEquals("Wrong label after selection","Selected: three", label.getText());

        tester.actionSelectRow(list, new JListLocation(0));
        assertEquals("Wrong label after selection","Selected: one", label.getText());
    }
}