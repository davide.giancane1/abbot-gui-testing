package abbot_gui_tests;

import abbot.finder.ComponentNotFoundException;
import abbot.finder.ComponentSearchException;
import abbot.finder.MultipleComponentsFoundException;
import abbot.finder.matchers.NameMatcher;
import abbot.tester.*;
import form_registration_example.GreetingsFrame;
import form_registration_example.RegistrationForm;
import junit.extensions.abbot.ComponentTestFixture;
import org.junit.Test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class RegistrationFormTest extends ComponentTestFixture {

    public RegistrationFormTest(String name) {
        super(name);
    }

    private RegistrationForm registrationForm;

    private ComponentTester mButtonTester, fButtonTester, nameTextTester, stateSpinnerTester;
    private ComponentTester finishButtonTester, checkButtonTester;
    private JTextComponentTester mobileTextTester;

    private JRadioButton male, female;
    private JTextField nameText, mobileText;
    private JSpinner stateSpinner;
    private JButton finishButton, checkButton;

    private Component getUIComponent(String componentName) throws ComponentNotFoundException, MultipleComponentsFoundException {
        return getFinder().find(registrationForm, new NameMatcher(componentName));
    }

    private void fetchGUIElements() throws ComponentNotFoundException, MultipleComponentsFoundException {
        male = (JRadioButton) getUIComponent("mButton");
        female = (JRadioButton) getUIComponent("fButton");
        nameText = (JTextField) getUIComponent("nameText");
        mobileText = (JTextField) getUIComponent("mobileText");
        stateSpinner = (JSpinner) getUIComponent("stateSpinner");
        finishButton = (JButton) getUIComponent("finishButton");
        checkButton = (JButton) getUIComponent("checkButton");
    }

    private void setTesters() {
        mButtonTester = new JButtonTester();
        fButtonTester = new JButtonTester();
        nameTextTester = new JTextFieldTester();
        mobileTextTester = new JTextComponentTester();
        stateSpinnerTester = new JSpinnerTester();
        finishButtonTester = new JButtonTester();
        checkButtonTester = new JButtonTester();
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        registrationForm = new RegistrationForm();
        getRobot().setAutoDelay(100);
        setTesters();
        fetchGUIElements();
    }

    @Override
    protected void showWindow(Window w) {
        super.showWindow(w);
        w.toFront();
    }

    public void testCorrectScenario() throws ComponentSearchException {
        insertNameAndAssertIt("Nicolò Paganini");
        selectSexAndAssert(male);

        //inserimento numero di telefono e semplice asserzione di verifica
        mobileTextTester.actionClick(mobileText);
        mobileTextTester.actionKeyString("3475104658");
        assertEquals("+393475104658", mobileText.getText());

        //controllo che i bottoni siano nello stato atteso
        checkButtonsState();

        checkInputValidation();
        finishAndAssertNewFrame();
    }

    public void testInputChangedScenario() throws ComponentSearchException {
        // showWindow(registrationForm, registrationForm.getSize());
        insertNameAndAssertIt("Queen Elizabeth II");
        selectSexAndAssert(female);
        insertTelWithPrefixAndAssert("+443457861290");
        checkButtonsState();
        selectStateAndAssertIt("Great Britain (GB)", 7);
        checkInputValidation();
        finishAndAssertNewFrame();
    }

    private void checkButtonsState() {
        assertTrue(checkButton.isEnabled());
        assertFalse(finishButton.isEnabled());
    }

    public void testAllInputMissing() throws ComponentNotFoundException, MultipleComponentsFoundException {
        checkButtonTester.actionClick(checkButton);
        checkInputMissing("Name");
        checkInputMissing("Sex");
        checkInputMissing("Mobile Phone");
    }

    @Test
    public void testNameFieldMissing() throws ComponentNotFoundException, MultipleComponentsFoundException {
        selectSexAndAssert(male);
        insertATelephone("3475104658");
        checkButtonTester.actionClick(checkButton);
        checkInputMissing("Name");
        checkErrorNotContains("Mobile Phone");
        checkErrorNotContains("Sex");
    }

    @Test
    public void testSexFieldMissing() throws ComponentNotFoundException, MultipleComponentsFoundException {
        insertNameAndAssertIt("Davide");
        insertATelephone("3475104658");
        checkButtonTester.actionClick(checkButton);
        checkInputMissing("Sex");
        checkErrorNotContains("Mobile Phone");
        checkErrorNotContains("Name");
    }

    public void testPhoneFieldMissing() throws ComponentNotFoundException, MultipleComponentsFoundException {
        insertNameAndAssertIt("Andrea");
        selectSexAndAssert(male);
        checkButtonTester.actionClick(checkButton);
        checkInputMissing("Mobile Phone");
        checkErrorNotContains("Sex");
        checkErrorNotContains("Name");
    }

    private void checkErrorNotContains(String notShown) throws ComponentNotFoundException, MultipleComponentsFoundException {
        JOptionPane errorPane = (JOptionPane) getFinder().find(c -> c instanceof JOptionPane);
        String errorMessage = (String) errorPane.getMessage();
        assertFalse(errorMessage.contains(notShown));

    }

    private void checkInputMissing(String whatIsMissing) throws ComponentNotFoundException, MultipleComponentsFoundException {
        JDialog errorDialog = (JDialog) getFinder().find(c -> c instanceof JDialog);
        assertTrue(errorDialog.isShowing());
        assertEquals(errorDialog.getTitle(), "Error Message");
        JOptionPane errorPane = (JOptionPane) getFinder().find(c -> c instanceof JOptionPane);
        String errorMessage = (String) errorPane.getMessage();
        assertTrue(errorMessage.contains(whatIsMissing));
    }

    private void finishAndAssertNewFrame() throws ComponentNotFoundException, MultipleComponentsFoundException {
        finishButtonTester.actionClick(finishButton);
        GreetingsFrame newFrame = (GreetingsFrame) getFinder().find(c -> c instanceof GreetingsFrame);
        assertTrue(!registrationForm.isShowing() && newFrame.isShowing());
    }

    private void checkInputValidation() throws ComponentNotFoundException, MultipleComponentsFoundException {
        checkButtonTester.actionClick(checkButton);
        JDialog dialog = (JDialog) getFinder().find(c -> c instanceof JDialog);

        //System.out.println(dialog.getTitle());
        assertTrue(dialog.isShowing());
        assertEquals(dialog.getTitle(), "Message");
        System.out.println(dialog.getTitle());
        JButton okButton = (JButton) getFinder().find(c -> (c instanceof JButton) && ((JButton) c).getText().equals("OK"));
        ComponentTester okTester = new JButtonTester();
        okTester.actionClick(okButton);
        assertTrue(finishButton.isEnabled());
    }

    private void selectStateAndAssertIt(String state, int numberOfClicks) {
        for (int i = 0; i < numberOfClicks; i++) {
            stateSpinnerTester.actionKeyPress(stateSpinner, KeyEvent.VK_UP);
            stateSpinnerTester.actionKeyRelease(stateSpinner, KeyEvent.VK_UP);
        }
        assertEquals(state, String.valueOf(stateSpinner.getValue()));
    }

    private void insertTelWithPrefixAndAssert(String number) {
        mobileTextTester.actionClick(mobileText);
        mobileTextTester.actionSelectText(mobileText, 0, mobileText.getText().length());
        mobileTextTester.actionKeyString(number);
        assertEquals(number, mobileText.getText());
    }

    private void selectSexAndAssert(JRadioButton radioButton) {
        if (radioButton.getName().equals("fButton")) {
            fButtonTester.actionClick(female);
            assertFalse(male.isSelected());
        } else {
            mButtonTester.actionClick(male);
            assertFalse(female.isSelected());
        }
        assertTrue(radioButton.isSelected());
    }

    private void insertNameAndAssertIt(String s) {
        nameTextTester.actionClick(nameText);
        nameTextTester.actionKeyString(s);
        assertEquals(s, nameText.getText());
        assertTrue(male.isEnabled());
        assertTrue(female.isEnabled());
    }

    private void insertATelephone(String number) {
        mobileTextTester.actionClick(mobileText);
        mobileTextTester.actionKeyString(number);
    }
}
