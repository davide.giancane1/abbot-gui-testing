package abbot_gui_tests;

import junit.extensions.abbot.ScriptFixture;
import junit.extensions.abbot.ScriptTestSuite;

import java.io.File;

public class TestScripts extends ScriptFixture {

    public TestScripts(String name) { super(name); }

    public static ScriptTestSuite suite() {

        String filePath = new File("").getAbsolutePath();
        filePath += File.separator + "scripts";

        return new ScriptTestSuite(TestScripts.class, filePath) {
            public boolean accept(File file) {
                String name = file.getName();
                return name.startsWith("test") && name.endsWith(".xml");
            }
        };
    }
}
