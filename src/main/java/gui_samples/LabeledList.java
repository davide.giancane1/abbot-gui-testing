package gui_samples;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;

public class LabeledList extends JPanel {

    private JList list;
    private JLabel label;

    public LabeledList(String[] initialContents) {

        setLayout(new BorderLayout());
        list = new JList(initialContents);
        add(list, BorderLayout.CENTER);
        label = new JLabel("Selected: ");
        //label.setName("etichetta");
        add(label, BorderLayout.SOUTH);

        list.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent lse) {
                label.setText("Selected: " + list.getSelectedValue());
            }
        });
    }
}