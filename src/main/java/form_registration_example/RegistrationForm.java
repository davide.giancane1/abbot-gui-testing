package form_registration_example;

import javax.swing.*;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RegistrationForm extends JFrame {

    // Dichiarazione elementi di interfaccia
    private JLabel titleLabel, nameLabel, genderLabel, mobileLabel, stateLabel;
    private JRadioButton mButton, fButton;
    private JTextField nameText, mobileText;
    private JSpinner stateSpinner;
    private JPanel contentPane;
    private JButton finishButton, checkButton;
    private String errorMessage;

    public RegistrationForm() {
        super();
        initializeComponent();
        this.setVisible(true);
    }

    private void initializeComponent() {
        titleLabel = new JLabel();
        nameLabel = new JLabel();
        genderLabel = new JLabel();
        mobileLabel = new JLabel();
        stateLabel = new JLabel();

        mButton = new JRadioButton();
        fButton = new JRadioButton();

      //  optionPane = new JOptionPane("Error");

        nameText = new JTextField();
        mobileText = new JTextField();
        mobileText.setText("+39");

        finishButton = new JButton("Finish");
        finishButton.setEnabled(false);
        checkButton = new JButton("Check");
        checkButton.setEnabled(true);

        contentPane = (JPanel) this.getContentPane();

        String[] stateStrings = getStateStrings();
        stateSpinner = new JSpinner(new SpinnerListModel(stateStrings));

        titleLabel.setHorizontalAlignment(SwingConstants.LEFT);
        titleLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        titleLabel.setIconTextGap(10);
        titleLabel.setOpaque(true);
        titleLabel.setText("      Welcome to our new online store. Sign up to start shopping !");
        titleLabel.setMaximumSize(new Dimension(500, 15));
        titleLabel.setBackground(new Color(208, 212, 224));

        nameLabel.setText("Customer Name:");
        genderLabel.setText("Gender:");
        mobileLabel.setText("Mobile Phone:");
        stateLabel.setText("State:");

        mButton.setText("Male");
        mButton.setBackground(new Color(208, 212, 224));
        fButton.setText("Female");
        fButton.setBackground(new Color(208, 212, 224));

        finishButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                finishButton_mouseClicked(e);
            }
        });

        checkButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                checkButton_mouseClicked(e);
            }
        });

        mButton.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                mButton_itemStateChanged(e);
            }

        });

        mButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                mButton_mouseClicked(e);
            }

        });

        fButton.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                fButton_itemStateChanged(e);
            }

        });

        fButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                fButton_mouseClicked(e);
            }

        });

        nameText.setActionCommand("name");
        nameText.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                text_actionPerformed(e);
            }
        });

        mobileText.setActionCommand("mobile");
        mobileText.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                text_actionPerformed(e);
            }
        });

        // contentPane
        contentPane.setLayout(null);
        contentPane.setBackground(new Color(208, 212, 224));
        addComponent(contentPane, titleLabel, 60,40,800,25);
        addComponent(contentPane, nameLabel, 60,150,150,25);
        addComponent(contentPane, genderLabel, 60,190,150,25);
        addComponent(contentPane, mobileLabel, 60,230,150,25);
        addComponent(contentPane, stateLabel, 60,270,150,25);

        addComponent(contentPane, mButton, 255,187,100,25);
        addComponent(contentPane, fButton, 355,187,100,25);
        addComponent(contentPane, nameText, 255,150,185,25);
        addComponent(contentPane, mobileText, 255,227,185,25);
        addComponent(contentPane, stateSpinner, 255,267,185,25);

        addComponent(contentPane, checkButton, 400, 500, 100,25);
        addComponent(contentPane, finishButton, 500, 500, 100,25);
      //  contentPane.add(optionPane);

        this.setTitle("RegistrationForm - REGISTER NOW");
        this.setLocation(new Point(0, 0));
        this.setSize(new Dimension(693, 584));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //setAllIDs();
        setAllIDs();
    }

    //Associo ad ogni elemento della GUI un identificativo univoco (per reperirli poi nella classe di test)
    private void setAllIDs(){
        List<Field> fieldList = Arrays
                .asList(getClass().getDeclaredFields())
                .stream().filter(field -> !(field.getType().equals(JLabel.class) || field.getType().equals(String.class)))
                .collect(Collectors.toList());
        try {
            for(Field f : fieldList){
                Class fClass = f.getType();
                Method setNameMethod = fClass.getMethod("setName", new Class[]{String.class});
                String fieldName = f.getName();
                setNameMethod.invoke(f.get(this), fieldName);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo per verificare gli input forniti dall'utente
     * @param event
     */
    public void checkButton_mouseClicked(MouseEvent event) {

        if(checkButton.isEnabled()) {
            System.out.println("\njcheckButton_mouseClicked(MouseEvent e) called.");

            if (checkEmptyDetails()) {
                if(!isIntNumber(mobileText.getText())) {
                    JOptionPane.showMessageDialog(null,"Enter a valid mobile phone number", "Error Message", JOptionPane.ERROR_MESSAGE);
                    mobileText.setText("+39");
                }
                else {
                    JOptionPane.showMessageDialog(null, "Press Finish to complete your registration", "Message", JOptionPane.INFORMATION_MESSAGE);
                    finishButton.setEnabled(true);
                    checkButton.setEnabled(false);
                }
            }
            else
                JOptionPane.showMessageDialog(null,errorMessage, "Error Message", JOptionPane.ERROR_MESSAGE);
        }
    }

    private boolean checkEmptyDetails() {
        boolean error = true;
        errorMessage = "Fill in these fields to continue OR enter a valid\n";

        if(nameText.getText().equals("")) {
            errorMessage += "- Name\n";
            error = false;
        }

        if(!mButton.isSelected() && !fButton.isSelected()) {
            errorMessage += "- Sex\n";
            error = false;
        }

        if(mobileText.getText().equals("") || mobileText.getText().equals("+39") || !isIntNumber(mobileText.getText())) {
            errorMessage += "- Mobile Phone\n";
            error = false;
        }

        return error;

    }

    public void finishButton_mouseClicked(MouseEvent e) {

        if(finishButton.isEnabled()) {
            System.out.println("\njnextButton_mouseClicked(MouseEvent e) called.");
            this.dispose();
            new GreetingsFrame();
        }
    }

    private void text_actionPerformed(ActionEvent e) {

        String cmd = e.getActionCommand();
        System.out.println(cmd);

        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.focusNextComponent();
    }

    private void mButton_mouseClicked(MouseEvent e) {
        System.out.println("\njRadioButton1_mouseClicked(MouseEvent e) called.");
        mButton.setEnabled(true);
        //fButton.setEnabled(false);
        fButton.setSelected(false);
    }

    private void mButton_itemStateChanged(ItemEvent e) {

        System.out.println("\njRadioButton1_itemStateChanged(ItemEvent e) called.");
        System.out.println(">>" + ((e.getStateChange() == ItemEvent.SELECTED) ? "selected":"unselected"));
        fButton.setSelected(false);
    }

    private void fButton_itemStateChanged(ItemEvent e) {

        System.out.println("\njRadioButton2_itemStateChanged(ItemEvent e) called.");
        System.out.println(">>" + ((e.getStateChange() == ItemEvent.SELECTED) ? "selected":"unselected"));
        mButton.setSelected(false);
    }

    private void fButton_mouseClicked(MouseEvent e) {

        System.out.println("\njRadioButton2_mouseClicked(MouseEvent e) called.");
        fButton.setEnabled(true);
        //mButton.setEnabled(false);
        mButton.setSelected(false);
    }

    private void addComponent(Container container,Component c,int x,int y,int width,int height) {
        c.setBounds(x,y,width,height);
        container.add(c);
    }


    public String[] getStateStrings() {
        String[] stateStrings = {
                "Italy (IT)", "United States Of America (USA)", "France (FR)", "Albania (AL)",
                "Spain (SP)", "Netherland (NL)", "Germany (DE)", "Great Britain (GB)", "Brazil (BZ)"
        };
        return stateStrings;
    }

    public boolean isIntNumber(String num) {
        try {
            Long.parseLong(num);
        }
        catch(NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    //Un metodo di utility per la creazione di una MaskFormatter
    protected MaskFormatter createFormatter(String s) {

        MaskFormatter formatter = null;
        try {
            formatter = new MaskFormatter(s);
        }
        catch (java.text.ParseException exc) {
            System.err.println("formatter is bad: " + exc.getMessage());
            System.exit(-1);
        }
        return formatter;
    }

    /*

    private void setAllIDs() {
        mButton.setName("mButton");
        fButton.setName("fButton");
        nameText.setName("nameText");
        mobileText.setName("mobileText");
        stateSpinner.setName("stateSpinner");
        contentPane.setName("contentPane");
        finishButton.setName("finishButton");
        checkButton.setName("checkButton");
        contentPane.setName("contentPane");
      //  optionPane.setName("optionPane");
    } */


    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
        new RegistrationForm();
    }
}
