package form_registration_example;

import java.awt.*;
import javax.swing.*;

public class GreetingsFrame extends JFrame
{
    private JLabel jLabel1;
    private JPanel contentPane;

    public GreetingsFrame()
    {
        super();
        initializeComponent();
        this.setVisible(true);
    }

    private void initializeComponent()
    {
        jLabel1 = new JLabel();
        jLabel1.setHorizontalAlignment(SwingConstants.LEFT);
        jLabel1.setHorizontalTextPosition(SwingConstants.CENTER);
        jLabel1.setIconTextGap(10);
        jLabel1.setOpaque(true);
        jLabel1.setText("      Thank you for registering.");
        jLabel1.setMaximumSize(new Dimension(500, 15));

        contentPane = (JPanel)this.getContentPane();
        contentPane.setLayout(null);
        contentPane.setBackground(new Color(208, 212, 224));
        addComponent(contentPane, jLabel1, 0,21,800,97);

        this.setTitle("Registration completed");
        this.setLocation(new Point(0, 0));
        this.setSize(new Dimension(693, 584));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void addComponent(Container container,Component c,int x,int y,int width,int height)
    {
        c.setBounds(x,y,width,height);
        container.add(c);
    }
}
